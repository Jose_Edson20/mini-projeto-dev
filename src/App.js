import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Home } from './Components/Home';
import { AddProject } from './Components/AddProject';
import { EditProject } from './Components/EditProject';
import {GlobalProvider} from './Context/GlobalState';

import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <div style = {{maxWidth: "30rem", margin: "4rem auto"}}>
      <GlobalProvider>
        <Router>
          <Switch>
            <Route exact path = "/" component = {Home} />
            <Route path = "/add" component = {AddProject} />
            <Route path = "/edit/:id" component = {EditProject} />
          </Switch>
        </Router>
     </GlobalProvider>
       </div>
  );
}

export default App;
