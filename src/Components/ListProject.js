import React, { useContext } from 'react';
import { GlobalContext } from '../Context/GlobalState';
import { Link } from 'react-router-dom';
import {
    ListGroup,
    ListGroupItem,
    Button
}from 'reactstrap';

export const ListProject = () => {
  const { projects, removeProject } = useContext(GlobalContext);

    return (
        <ListGroup className = "mt-4">
          {projects.length > 0 ?(
            <>
          {projects.map(project => (
               <ListGroupItem className = "d-flex">

                 <strong>{project.name}</strong>
     
                 <div className = "ml-auto">
                   <Link className = "btn btn-warning mr-1" to = {`/edit/ ${project.id}`}>Edit</Link>
                   <Button onClick ={() => removeProject(project.id) } 
                    color = "danger">Delete</Button>
     
                 </div>
               </ListGroupItem>
          ))}
          </>
          ) :(
              <h3>No Projects</h3>
          )}

          </ListGroup>
    )
}
/* export default ListProject; */