import React, {useState, useContext} from 'react';
import { GlobalContext } from '../Context/GlobalState';
import { v4 as uuid } from 'uuid';
import { Link, useHistory } from 'react-router-dom';
import {
    Form,
    FormGroup,
    Label,
    Input,
    Button, 
    ButtonToolbar,
    ButtonGroup,
    Navbar,
    Nav,
    NavItem,
    NavbarBrand,
    Container
} from 'reactstrap'


export const AddProject = () => {
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');

    const {addProject } = useContext(GlobalContext);
    const history = useHistory();

    const onSubmit = (e) => {
        const newProject = {
            id: uuid(), 
            name,
            description
        }
        addProject(newProject)
        history.push('/');
    }

    const onChange = (e) => {
        setName(e.target.value)
    }

    const OnChange = (e) => {
        setDescription(e.target.value)
    }

    return (


        <Form onSubmit = {onSubmit}>
            <FormGroup>
                <Label>Name Project:</Label>
                <Input type = "text" value = {name} onChange={onChange} name= "name"
                     placeholder = "Name project" required></Input> <br/>

                 <Label>Description Project:</Label>
                <Input type = "text" value = {description} onChange={OnChange} name= "description"
                     placeholder = "project description" required></Input> <br/>

                   
                   <Label>Viability:</Label> <br/>
                 <Input type="radio" id="viability" name="viability" value="one" required></Input>
                <Label>1</Label> <br/>

                 <Input type="radio" id="viability" name="viability" value="two" required></Input>
                <Label>2</Label><br/>

                 <Input type="radio" id="viability" name="viability" value="three" required></Input>
                <Label>3</Label> <br/>
        
                 <Input type="radio" id="viability" name="viability" value="four" required></Input>
                <Label>4</Label> <br/>
        
                 <Input type="radio" id="viability" name="viability" value="five" required></Input>
                <Label>5</Label><br/>

    

                 <Label>Date Start of Project:</Label>
                <Input type = "date" id="date" name="date"  required></Input> <br/>

                 <Label>Date End of Project:</Label>
                <Input type = "date" id="date" name="date"  required></Input> 
                        <br/>

                <ButtonToolbar aria-label="Toolbar with button groups">
                    <ButtonGroup className="mr-2" aria-label="First group">
                    <Button required>Planejado</Button> 
                    <Button color = "warning" required>Em Desenvolvimento</Button> 
                    <Button color = "danger" required>Cancelado</Button> 
                    <Button color = "success" required>Concluído</Button>
                    </ButtonGroup>
                </ButtonToolbar>

            </FormGroup>
            <Button color = "primary" type ="submit">Add Project</Button>
            <Link to  = "/" className = "btn btn-danger ml-2">Cancel</Link>
        </Form>



    )
 }

export default AddProject