import React, {useState, useContext, useEffect} from 'react';
import { GlobalContext } from '../Context/GlobalState';
import { Link, useHistory } from 'react-router-dom';
 
import {
    Form,
    FormGroup,
    Label,
    Input,
    Button,
    Navbar,
    Nav,
    NavItem,
    NavbarBrand,
    Container
} from 'reactstrap'

export const EditProject = (props) => {

    const { editProject, projects } = useContext(GlobalContext);
    const [selectedProject, setSelectedProject] = useState({
        id: '',
        name: ''
    })
    const [selectedDescription, setSelectedDescription] = useState({
        description: ''
    })
   
    const history = useHistory();
    const currentProjectId = props.match.params.id;

    useEffect (() =>{
        const projectId = currentProjectId
        const selectedProject = projects.find(project => project.id === projectId)
        setSelectedProject(selectedProject)
    }, [currentProjectId, projects])


    const onChange = (e) => {
        setSelectedProject({...selectedProject, [e.target.name]: e.target.value}
    )}

    const onSubmit = (e) => {
        editProject(selectedProject)
        history.push("/");
    }
  
    return (

        <Form onSubmit ={onSubmit}>
            <FormGroup>
                <Label>Name Project:</Label>
                <Input type ="text" onChange={onChange} 
                  name="name" placeholder = "Edit project" required></Input> <br/>

                <Label>Description Project:</Label>
                <Input type ="text" onChange={onChange}
                  name="name" placeholder = "Edit project" required></Input>

            </FormGroup>
            <Button color = "warning" type ="submit">Edit Project</Button>
            <Link to  = "/" className = "btn btn-danger ml-2">Cancel</Link>
        </Form>
    )
}
export default EditProject;