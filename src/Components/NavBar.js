import React from 'react';
import { Link } from 'react-router-dom';
import {
    Navbar,
    Nav,
    NavItem,
    NavbarBrand,
    Container
} from 'reactstrap';

export const NavBar = () => {
    return (
        <Navbar color = "dark" dark>
            <Container>
                <NavbarBrand href = "/"> Todo App </NavbarBrand>
                <Nav>
                    <NavItem>
                        <Link className = "btn btn-primary" to = "/add" >Add Project</Link>
                    </NavItem>
                </Nav>
            </Container>
        </Navbar>
    )
}
export default NavBar;