import React from 'react';
import { NavBar } from './NavBar'
import { ListProject } from './ListProject'

export const Home = () => {
    return (
        <>
            <NavBar />
            <ListProject />
        </>
    )
}
export default Home;