export default (state, action) => {
    switch(action.type) {
        case 'REMOVE_PROJECT':
            return {
                ...state,
                projects: state.projects.filter(projects =>{
                    return projects.id !== action.payload
                })
            }
         case 'ADD_PROJECT':
             return {
                 ...state,
                 projects: [action.payload, ...state.projects]
             }

         case 'EDIT_PROJECT':
             const updateProject = action.payload

             const updateProjects = state.projects.map(project =>{
                 if(project.id === updateProject.id ){
                     return updateProject;
                 }
                 return project;
             })

             return {
                 ...state,
                projects: updateProjects
             }

        default:
            return state;
    }
}