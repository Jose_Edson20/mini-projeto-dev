import AppState from './AppState';
import { createContext, useReducer } from 'react';

const initialState = {
    projects: [    
        {id : 1, name: 'Project DataBase'},
        {id : 2, name: 'Project Data Science'},
        {id : 3, name: 'Project Enginer Software'},
        {id : 4, name: 'Project UX and UI designer'},
        {id : 5, name: 'Project Power BI'},
        {id : 6, name: 'Project IA'},
    ] 
};
export const GlobalContext = createContext(initialState);

export const GlobalProvider = ({children}) => {
    const [state, dispatch] = useReducer (AppState, initialState);


    const removeProject = (id) => {
        dispatch ({
            type: 'REMOVE_PROJECT',
            payload: id
        })
    }

    const addProject = (project) => {
        dispatch ({
            type: 'ADD_PROJECT',
            payload: project
        })
    }

    const editProject = (project) => {
        dispatch ({
            type: 'EDIT_PROJECT',
            payload: project
        })
    }

    return (
        <GlobalContext.Provider value = {{
            projects : state.projects,
            removeProject,
            addProject,
            editProject
        }}>
            {children}
        </GlobalContext.Provider>
    )
}